### Task Description
Create a RESTful API to manage a simple todo list application using Spring Boot, Hibernate, and PostgreSQL. 
The application should allow users to create, read, update, and delete todo items.
Each item should have a title and a description. 
Use Hibernate to persist the items in the database.

### Configs
* Setup PostgreSQL
    * Create a database with name todo_list 
    * Select created database 
    * Execute *test/resources/init_test_state.sql* folder or simply run tests
    * Setup PostgreSQL credentials in *java/resources/application.yml* file.
  
### Chat
You can find chat history by [this link](https://chat.openai.com/share/cf2434a8-1443-43ed-96a4-5cb1963796e1) or in a chat.log.




