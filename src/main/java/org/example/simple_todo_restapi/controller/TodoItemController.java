package org.example.simple_todo_restapi.controller;

import jakarta.persistence.EntityNotFoundException;
import org.example.simple_todo_restapi.entity.TodoItem;
import org.example.simple_todo_restapi.service.TodoItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/todo")
public class TodoItemController {
    private final TodoItemService todoItemService;

    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @GetMapping("/")
    public ResponseEntity<List<TodoItem>> getAllItems() {
        List<TodoItem> todoItems = todoItemService.getAllItems();
        return new ResponseEntity<>(todoItems, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TodoItem> getItemById(@PathVariable Long id) {
        TodoItem todoItem = todoItemService.getItemById(id);
        return todoItem != null ?
                new ResponseEntity<>(todoItem, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    public ResponseEntity<TodoItem> addItem(@RequestBody TodoItem item) {
        TodoItem savedItem = todoItemService.saveItem(item);
        return new ResponseEntity<>(savedItem, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TodoItem> updateItem(@PathVariable Long id, @RequestBody TodoItem item) {
        TodoItem updatedItem = todoItemService.updateItem(id, item);
        return updatedItem != null ?
                new ResponseEntity<>(updatedItem, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteItem(@PathVariable Long id) {
        todoItemService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
