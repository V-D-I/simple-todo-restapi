package org.example.simple_todo_restapi.repository;


import org.example.simple_todo_restapi.entity.TodoItem;
//import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

//public interface TodoItemRepository extends JpaRepository<TodoItem, Long> {
//
//}

public interface TodoItemRepository {
    List<TodoItem> findAll();

    Optional<TodoItem> findById(Long id);

    TodoItem save(TodoItem item);

    TodoItem update(TodoItem item);

    void deleteById(Long id);
}