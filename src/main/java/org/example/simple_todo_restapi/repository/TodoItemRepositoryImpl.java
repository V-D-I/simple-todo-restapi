package org.example.simple_todo_restapi.repository;

import org.example.simple_todo_restapi.entity.TodoItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class TodoItemRepositoryImpl implements TodoItemRepository {
    private final SessionFactory sessionFactory;

    public TodoItemRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TodoItem> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM TodoItem", TodoItem.class).getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TodoItem> findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        TodoItem todoItem = session.get(TodoItem.class, id);

        return todoItem != null ?
                Optional.of(todoItem) :
                Optional.empty();
    }

    @Override
    @Transactional
    public TodoItem save(TodoItem item) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(item);
        return item;
    }

    @Override
    @Transactional
    public TodoItem update(TodoItem item) {
        Session session = sessionFactory.getCurrentSession();
        session.merge(item);
        return item;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        TodoItem item = session.get(TodoItem.class, id);
        if (item != null) {
            session.remove(item);
        }
    }
}
