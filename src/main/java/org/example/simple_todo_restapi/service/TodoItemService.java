package org.example.simple_todo_restapi.service;

import org.example.simple_todo_restapi.entity.TodoItem;
import org.example.simple_todo_restapi.repository.TodoItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoItemService {
    private final TodoItemRepository todoItemRepository;

    public TodoItemService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    public List<TodoItem> getAllItems() {
        return todoItemRepository.findAll();
    }

    public TodoItem getItemById(Long id) {
        return todoItemRepository.findById(id).orElse(null);
    }

    public TodoItem saveItem(TodoItem item) {
        return todoItemRepository.save(item);
    }

    public TodoItem updateItem(Long itemId, TodoItem item) {
        Optional<TodoItem> itemFromDb = todoItemRepository.findById(itemId);

        if (itemFromDb.isPresent()) {
            item.setId(itemId);
            return todoItemRepository.update(item);
        } else {
            return null;
        }
    }

    public void deleteItem(Long id) {
        todoItemRepository.deleteById(id);
    }
}
