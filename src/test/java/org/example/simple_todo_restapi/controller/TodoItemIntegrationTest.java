package org.example.simple_todo_restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.simple_todo_restapi.entity.TodoItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(scripts = "/init_test_state.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TodoItemIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testGetAllItems() throws Exception {
        mockMvc.perform(get("/api/todo/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testFindItemById() throws Exception {
        long itemId = 1L;

        mockMvc.perform(get("/api/todo/" + itemId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(itemId));
    }

    @Test
    public void testGetNonExistentItem() throws Exception {
        long nonExistentItemId = 9999L; // An ID that doesn't exist in the database

        mockMvc.perform(get("/api/todo/" + nonExistentItemId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testAddItem() throws Exception {
        TodoItem newItem = new TodoItem();
        newItem.setTitle("Test Task");
        newItem.setDescription("Test Description");

        mockMvc.perform(post("/api/todo/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newItem)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Test Task"))
                .andExpect(jsonPath("$.description").value("Test Description"));
    }

    @Test
    public void testUpdateItem() throws Exception {
        TodoItem updatedItem = new TodoItem();
        updatedItem.setTitle("Updated Task");
        updatedItem.setDescription("Updated Description");

        mockMvc.perform(put("/api/todo/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedItem)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Updated Task"))
                .andExpect(jsonPath("$.description").value("Updated Description"));
    }

    @Test
    public void testUpdateNonExistentItem() throws Exception {
        long nonExistentItemId = 9999L; // An ID that doesn't exist in the database
        TodoItem updatedItem = new TodoItem();
        updatedItem.setTitle("Updated Task");
        updatedItem.setDescription("Updated Description");

        mockMvc.perform(put("/api/todo/{id}", nonExistentItemId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedItem)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteItem() throws Exception {
        mockMvc.perform(delete("/api/todo/{id}", 1L))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteNonExistentItem() throws Exception {
        long nonExistentItemId = 9999L; // An ID that doesn't exist in the database

        mockMvc.perform(delete("/api/todo/{id}", nonExistentItemId))
                .andExpect(status().isNoContent());
    }
}
