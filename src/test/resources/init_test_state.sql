DROP TABLE IF EXISTS todo_items;

-- Create the todo_items table
CREATE TABLE todo_items
(
    id          SERIAL PRIMARY KEY,
    title       VARCHAR(255) NOT NULL,
    description TEXT
);

-- Insert some sample data
INSERT INTO todo_items (title, description)
VALUES ('Task 1', 'Description for Task 1'),
       ('Task 2', 'Description for Task 2'),
       ('Task 3', 'Description for Task 3');